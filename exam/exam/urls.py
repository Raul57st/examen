
from django.contrib import admin
from django.urls import path
from examAPP import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.home, name="home"),

    path('NFLP/jugadores/', views.jugadores, name="jugadores"),
    path('NFLP/jugadores/agregar/', views.agregar_jugador, name="agregar_jugador"),
    path('NFLP/jugadores/agregar2/', views.agregar_equipo, name="agregar_equipo"),
    path('NFLP/jugadores/agregar3/', views.agregar_estadio, name="agregar_estadio"),
    path('NFLP/jugadores/list/', views.lista, name="lista"),
    path('NFLP/jugadores/listEquipos/', views.lista_equipos, name="lista_equipos"),
    path('NFLP/jugadores/listaEstadios/', views.lista_estadios, name="lista_estadios"),
    path('NFLP/jugadores/detail/<int:id>/', views.detail, name="detail"),
    
]
