from django import forms

from .models import jugadore, equipo, estadio

class jugadorForm(forms.ModelForm):
    class Meta:
        model = jugadore
        fields = [
            "name",
            "number",
        ]

class estadioForm(forms.ModelForm):
    class Meta:
        model = estadio
        fields = [
            "nombre",
            "capacidad",
        ]

class equipoForm(forms.ModelForm):
    class Meta:
        model = equipo
        fields = [
            "nombre",
        ]
