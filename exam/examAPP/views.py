from django.shortcuts import render, redirect

# Create your views here.

from .models import jugadore, equipo, estadio
from .forms import jugadorForm, estadioForm, equipoForm


def home(request):
    context = {
        "mensaje": "Un mensaje xD!!"
    }
    return render(request, 'home.html', context)

def jugadores(request):
    return render(request, 'jugadores.html',)

def agregar_jugador(request):
    return render(request, 'agregar_jugador.html',)

#Agregar jugadores
def agregar_jugador(request):
    form = jugadorForm()

    if request.method == "POST":
        form = jugadorForm(request.POST)

        if form.is_valid():
            jugadores = form.save(commit = False)
            jugadores.save()
            return redirect(agregar_jugador)
    return render(request, 'agregar_jugador.html', {
        "form": form
    })
#end Agregar jugadores

#Agregar equipos
def agregar_equipo(request):
    form = equipoForm()

    if request.method == "POST":
        form = equipoForm(request.POST)

        if form.is_valid():
            jugadores = form.save(commit = False)
            jugadores.save()
            return redirect(agregar_equipo)
    return render(request, 'agregar_equipo.html', {
        "form": form
    })
#end Agregar equipos


#Agregar estadios
def agregar_estadio(request):
    form = estadioForm()

    if request.method == "POST":
        form = estadioForm(request.POST)

        if form.is_valid():
            jugadores = form.save(commit = False)
            jugadores.save()
            return redirect(agregar_estadio)
    return render(request, 'agregar_estadio.html', {
        "form": form
    })
#end Agregar equipos



#lista de jugadores
def lista(request):
    context = {
        "lista_jugadores": jugadore.objects.all()
    }
    return render(request, "list.html", context)

#end lista de jugadores

#Lista de equipos

def lista_equipos(request):
    context = {
        "lista_jugadores": equipo.objects.all()
    }
    return render(request, "listEquipos.html", context)
#end lista de equipos

#lista de estadios
def lista_estadios(request):
    context = {
        "lista_estadios": estadio.objects.all()
    }
    return render(request, "listaEstadios.html", context)

#end lista de estadios

#detail jugadores
def detail(request, id):
    queryset = jugadore.objects.get(id=id)
    context = {
        "object": queryset
    }
    return render(request, "detail.html", context)

# #detail estadios
# def detail_estadios(request, id):
#     queryset = estadio.objects.get(id=id)
#     context = {
#         "object": queryset
#     }
#     return render(request, "detail_estadios.html", context)
#
# #end detail estadios
#
# #detail equipos
# def detail_equipos(request,id):
#     queryset = equipo.objects.get(id=id)
#     context = {
#         "object": queryset
#    }

#Create

 # def create(request):
 #    form = jugadorForm(request.POST or None)
 #    if request.user.is_authenticated:
 #        error = "User Logged"
 #        if form.is_valid():
 #            form.save()
 #    else:
 #        error = "Uswer Must Be Logged"
 #
 #    context = {
 #        "form": form,
 #        "error": error
 #    }
 #    return render(request, "create.html")
