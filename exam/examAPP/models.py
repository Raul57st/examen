from django.db import models

# Create your models here.

class jugadore(models.Model):
    name = models.CharField(max_length=32)
    number = models.IntegerField()

    def __str__(self):
        return self.name

class equipo(models.Model):
    nombre = models.CharField(max_length=32)

    def __str__(self):
        return self.nombre

class estadio(models.Model):
    nombre = models.CharField(max_length=32)
    capacidad = models.IntegerField()

    def __str__(self):
        return self.nombre
